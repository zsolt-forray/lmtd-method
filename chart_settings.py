#!/usr/bin/python3


"""
Chart settings
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '24/11/2019'
__status__  = 'Development'


import os
import json
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button


class ChartSettings:
    def __init__(self):
        # JSON file location
        self.json_loc = os.path.join(os.path.dirname(os.path.abspath(__file__)), "json")
        # Label axes position
        self.text_axes_pos = (0.055, 0.15, 0.5, 0.1)

    @staticmethod
    def set_axes(*pos):
        axes = plt.axes([*pos])
        return axes

    @staticmethod
    def set_sliders_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return Slider(ChartSettings.set_axes(*positions), params["text"],\
                                    params["min"], params["max"],\
                                    params["init"], params["valfmt"],\
                                    color=params["color"])

    @staticmethod
    def set_buttons_params(params):
        positions = params["left"], params["bottom"], params["width"], params["height"]
        return Button(ChartSettings.set_axes(*positions), params["text"],\
                                    color=params["color"], hovercolor=params["hovercolor"])

    def set_labels_params(self, params, text):
        positions = params["x"], params["y"]
        return self.ax_text.text(*positions, text, size=params["size"],\
                                 style=params["style"], color=params["color"],\
                                 backgroundcolor=params["bgcolor"],\
                                 weight=params["weight"])

    def set_sliders(self):
        # Sliders
        with open(os.path.join(self.json_loc, "sliders_parameters.json")) as json_file:
            slider_params = json.load(json_file)

        # Sliders for hot oil inlet temperature
        T1_in_sl_params = slider_params["T1_in_sl"]
        self.T1_in_sl = ChartSettings.set_sliders_params(T1_in_sl_params)

        # Sliders for cold water inlet temperature
        T2_in_sl_params = slider_params["T2_in_sl"]
        self.T2_in_sl = ChartSettings.set_sliders_params(T2_in_sl_params)

        # Sliders for cold oil outlet temperature
        T1_out_sl_params = slider_params["T1_out_sl"]
        self.T1_out_sl = ChartSettings.set_sliders_params(T1_out_sl_params)

        # Sliders for hot oil mass flow rate
        mw_params = slider_params["mw"]
        self.mw = ChartSettings.set_sliders_params(mw_params)

        # Sliders for cold water mass flow rate
        mc_params = slider_params["mc"]
        self.mc = ChartSettings.set_sliders_params(mc_params)

    def set_buttons(self):
        # Buttons
        with open(os.path.join(self.json_loc, "button_parameters.json")) as json_file:
            button_params = json.load(json_file)

        # Button for counter flow type selection
        counter_button_params = button_params["counter"]
        self.counter_button = ChartSettings.set_buttons_params(counter_button_params)

        # Button for parallel flow type selection
        parallel_button_params = button_params["parallel"]
        self.parallel_button = ChartSettings.set_buttons_params(parallel_button_params)

        # Button for exit
        exit_button_params = button_params["exit"]
        self.exit_button = ChartSettings.set_buttons_params(exit_button_params)

    def set_labels(self):
        self.ax_text = ChartSettings.set_axes(*self.text_axes_pos)
        self.ax_text.axis("off")

        # Labels
        with open(os.path.join(self.json_loc, "label_parameters.json")) as json_file:
            label_params = json.load(json_file)

        flow_type_params = label_params["flow_type"]
        self.set_labels_params(flow_type_params, "Flow type")

        hot_stream_params = label_params["hot_stream"]
        self.set_labels_params(hot_stream_params, "Hot stream (oil)")

        cold_stream_params = label_params["cold_stream"]
        self.set_labels_params(cold_stream_params, "Cold stream (water)")

        inlet_hot_params = label_params["inlet_hot"]
        self.set_labels_params(inlet_hot_params, "Inlet temperature, °C")

        outlet_hot_params = label_params["outlet_hot"]
        self.set_labels_params(outlet_hot_params, "Outlet temperature, °C")

        hot_mass_params = label_params["hot_mass"]
        self.set_labels_params(hot_mass_params, "Mass flow rate, kg/s")

        inlet_cold_params = label_params["inlet_cold"]
        self.set_labels_params(inlet_cold_params, "Inlet temperature, °C")

        cold_mass_params = label_params["cold_mass"]
        self.set_labels_params(cold_mass_params, "Mass flow rate, kg/s")

        result_label_params = label_params["result_label"]
        self.set_labels_params(result_label_params, "Results")

        msg_label_params = label_params["msg_label"]
        self.msg_text = self.set_labels_params(msg_label_params, "")

        note_label_params = label_params["note_label"]
        note_label_text = ("Note - Overall heat transfer coefficient: 250 W/m2°C, "
                           "specific heat (oil / water): 2.5 / 4.2 kJ/kg°C")
        self.set_labels_params(note_label_params, note_label_text)

        result_other_params = label_params["result_other"]
        self.res_text = ("Water outlet temp. (°C): {:>7}\n\n"
                         "LMTD (°C): {:>28}\n\n"
                         "Total heat transfer (kW): {:>7}\n\n\n")
        self.tbox = self.set_labels_params(result_other_params, self.res_text\
                                           .format(self.T_cold_out, self.LMTD, self.q_kw))

        result_surface_params = label_params["result_surface"]
        self.res_area = "Surface area (m2): {:>11}"
        self.tbox_area = self.set_labels_params(result_surface_params, self.res_area\
                                                .format(self.A))

    def set_chart(self):
        self.fig = plt.figure(figsize=(9,6))
        self.ax = self.fig.add_subplot(1, 1, 1)
        self.ax.axis("off")
        # Widgets
        self.set_sliders()
        self.set_buttons()
        self.set_labels()
