# HEX Surface Calculation - LMTD-method

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/582cf1425e354c10ae5912c5d38c3e55)](https://www.codacy.com/manual/forray.zsolt/lmtd-method?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=Zsolt-Forray/lmtd-method&amp;utm_campaign=Badge_Grade)
[![Python 3.7](https://img.shields.io/badge/python-3.7-blue.svg)](https://www.python.org/downloads/release/python-370/)

## Description
This tool determines the surface area of (counter / parallel flow) heat exchanger using the LMTD method.

## Usage

![Screenshot](/png/fig.png)

### Usage Example

```python
#!/usr/bin/python3

import lmtd_method as lm

lm.run_app()
```

**Initial Parameters:**

* Flow tye (counter / parallel)
* Oil inlet temperature (°C)
* Oil outlet temperature (°C)
* Oil mass flow rate (kg/s)
* Oil specific heat (J/kg°C)
* Water inlet temperature (°C)
* Water mass flow rate (kg/s)
* Water specific heat (J/kg°C)
* Overall heat transfer coefficient (W/m2°C)

## LICENSE
MIT

## Contributions
Contributions to this repository are always welcome.
This repo is maintained by Zsolt Forray (forray.zsolt@gmail.com).
