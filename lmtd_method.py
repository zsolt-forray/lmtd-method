#!/usr/bin/python3


"""
This tool determines the surface area of a heat exchanger using the LMTD method.
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '22/11/2019'
__status__  = 'Development'


import math
from chart_plotting import ChartPlotting


class LMTDMethod(ChartPlotting):
    def __init__(self):
        self.flow_type   = "counter"
        self.T_warm_in   = 90       # °C
        self.T_warm_out  = 30       # °C
        self.m_warm      = 4.0      # kg/s
        self.c_warm      = 2500     # J/kg°C
        self.T_cold_in   = 15       # °C
        self.m_cold      = 8.0      # kg/s
        self.c_cold      = 4197     # J/kg°C
        self.U           = 250      # W/m2°C
        self.CORR_FACTOR = 1.0      # Correction factor (for parallel / counterflow)

        super().__init__()

    def calc_capacity(self):
        # Capacity
        C_warm = self.m_warm * self.c_warm
        C_cold = self.m_cold * self.c_cold
        return C_warm, C_cold

    def calc_heat_transfer_rate(self, C_warm):
        # Heat Transfer Rate
        q = C_warm * (self.T_warm_in - self.T_warm_out)
        return round(q / 1000,1)

    def calc_outlet_temperature(self, C_cold, q_kw):
        # Outlet temperature
        return round(q_kw * 1000 / C_cold + self.T_cold_in,1)

    def data_on_error(self):
        self.A = "-"
        self.LMTD = "-"
        self.q_kw = "-"

    def check_conditions(self, T_cold_out):
        self.conditions = [
                self.T_warm_out >= self.T_warm_in,
                self.T_cold_in >= self.T_warm_in,
                (T_cold_out >= self.T_warm_out) & (self.flow_type == "parallel"),
                (T_cold_out >= self.T_warm_in) & (self.flow_type == "counter"),
                (self.T_cold_in >= self.T_warm_out) & (self.flow_type == "counter")
                ]

        if any(self.conditions):
            # Do not raise InvalidTempError(), the write_msg() does not work properly.
            # The conditions list does not update until the InvalidTempError not fixed.
            self.data_on_error()

    def write_msg(self):
        messages = [    "Oil outlet temperature >= Oil inlet temperature",
                        "Water inlet temperature >= Oil inlet temperature",
                        "Water outlet temperature >= Oil outlet temperature",
                        "Water outlet temperature >= Oil inlet temperature",
                        "Water inlet temperature >= Oil outlet temperature"
                        ]

        try:
            msg_index = self.conditions.index(True)
            self.msg = messages[msg_index]
        except ValueError:
            self.msg = ""

    def calc_lmtd(self, T_cold_out):
        if self.flow_type == "parallel":
            # Parallel flow
            DTa = self.T_warm_in - self.T_cold_in
            DTb = self.T_warm_out - T_cold_out
            LMTD = round((DTa - DTb) / math.log(DTa / DTb),1)
        elif self.flow_type == "counter":
            # Counter flow
            DTa = self.T_warm_in - T_cold_out
            DTb = self.T_warm_out - self.T_cold_in
            LMTD = round((DTa - DTb) / math.log(DTa / DTb),1)
        return LMTD

    def calc_surface(self, q_kw, LMTD):
        # Surface
        return round(q_kw * 1000 / (self.U * self.CORR_FACTOR * LMTD),1)

    @staticmethod
    def print_result(T_cold_out, A, LMTD, q_kw):
        print("\nResults\n\
               \nWater outlet temperature (°C): {}\
               \nSurface area (m2): {}\
               \nLMTD (°C): {}\
               \nTotal heat transfer (kW): {}\n"\
               .format(T_cold_out, A, LMTD, q_kw))

    def run_calc(self):
        try:
            C_warm, C_cold = self.calc_capacity()
            self.q_kw = self.calc_heat_transfer_rate(C_warm)
            self.T_cold_out = self.calc_outlet_temperature(C_cold, self.q_kw)
            self.LMTD = self.calc_lmtd(self.T_cold_out)
            self.A = self.calc_surface(self.q_kw, self.LMTD)

        except (ValueError, ZeroDivisionError):
            self.data_on_error()

        finally:
            self.check_conditions(self.T_cold_out)
            self.write_msg()
            LMTDMethod.print_result(self.T_cold_out, self.A, self.LMTD, self.q_kw)

        return self.T_cold_out, self.A, self.LMTD, self.q_kw


def run_app():
    lmtdobj = LMTDMethod()
    lmtdobj.run_calc()
    lmtdobj.run_plot()


if __name__ == "__main__":
    run_app()
