#!/usr/bin/python3


"""
Chart plotting
"""


__author__  = 'Zsolt Forray'
__license__ = 'MIT'
__version__ = '0.0.1'
__date__    = '24/11/2019'
__status__  = 'Development'


from matplotlib import pyplot as plt
from chart_settings import ChartSettings


class ChartPlotting(ChartSettings):
    def __init__(self):
        ChartSettings.__init__(self)

    def run_plot(self):
        self.set_chart()
        self.T1_in_sl.on_changed(self.update_temp)
        self.T2_in_sl.on_changed(self.update_temp)
        self.T1_out_sl.on_changed(self.update_temp)
        self.mw.on_changed(self.update_mass_flow)
        self.mc.on_changed(self.update_mass_flow)
        self.parallel_button.on_clicked(self.parallel_calc)
        self.counter_button.on_clicked(self.counter_calc)
        self.exit_button.on_clicked(ChartPlotting.close_chart)
        plt.show()

    def update_temp(self, val):
        self.T_warm_in = int(self.T1_in_sl.val)
        self.T_cold_in = int(self.T2_in_sl.val)
        self.T_warm_out = int(self.T1_out_sl.val)

        T_cold_out, A, LMTD, q_kw = self.run_calc()

        if self.msg != "":
            self.msg_text.set_text(self.msg)
        else:
            self.msg_text.set_text(self.msg)
        self.tbox.set_text(self.res_text.format(T_cold_out, LMTD, q_kw))
        self.tbox_area.set_text(self.res_area.format(A))
        self.fig.canvas.draw_idle()

    def update_mass_flow(self, val):
        self.m_warm = round(self.mw.val,1)
        self.m_cold = round(self.mc.val,1)

        T_cold_out, A, LMTD, q_kw = self.run_calc()

        if self.msg != "":
            self.msg_text.set_text(self.msg)
        else:
            self.msg_text.set_text(self.msg)
        self.tbox.set_text(self.res_text.format(T_cold_out, LMTD, q_kw))
        self.tbox_area.set_text(self.res_area.format(A))
        self.fig.canvas.draw_idle()

    def parallel_calc(self, evt):
        self.flow_type = "parallel"
        T_cold_out, A, LMTD, q_kw = self.run_calc()

        if self.msg != "":
            self.msg_text.set_text(self.msg)
        else:
            self.msg_text.set_text(self.msg)
        self.parallel_button.color = "#4FAF09"
        self.counter_button.color = "#C4CCD0"

        self.tbox.set_text(self.res_text.format(T_cold_out, LMTD, q_kw))
        self.tbox_area.set_text(self.res_area.format(A))
        self.fig.canvas.draw_idle()

    def counter_calc(self, evt):
        self.flow_type = "counter"
        T_cold_out, A, LMTD, q_kw = self.run_calc()

        if self.msg != "":
            self.msg_text.set_text(self.msg)
        else:
            self.msg_text.set_text(self.msg)
        self.parallel_button.color = "#C4CCD0"
        self.counter_button.color = "#4FAF09"

        self.tbox.set_text(self.res_text.format(T_cold_out, LMTD, q_kw))
        self.tbox_area.set_text(self.res_area.format(A))
        self.fig.canvas.draw_idle()

    @staticmethod
    def close_chart(evt):
        plt.close()
